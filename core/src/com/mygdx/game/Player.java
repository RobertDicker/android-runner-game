package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Shape2D;
import com.badlogic.gdx.utils.Disposable;
import com.mygdx.game.actors.GameObject;
import com.mygdx.game.actors.WorldMap;
import com.mygdx.game.behaviours.CollisionObject;
import com.mygdx.game.behaviours.ObjectMovementBehaviour;
import com.mygdx.game.stages.GameStage;

import static com.mygdx.game.Helpers.transform2DTextureRegionTo1D;

public class Player extends GameObject implements Disposable {


    //Running
    private static final int RUN_FRAME_COLS = 4;
    private static final int RUN_FRAME_ROWS = 2;
    //Jumping
    private static final int JUMP_START_FRAME_COLS = 3;
    private static final int JUMP_START_FRAME_ROWS = 1;
    private static final int JUMP_END_FRAME_COLS = 2;
    private static final int JUMP_END_FRAME_ROWS = 1;
    //Sliding
    private static final int SLIDE_START_FRAME_COLS = 2;
    private static final int SLIDE_START_FRAME_ROWS = 1;
    private static final int SLIDE_END_FRAME_COLS = 1;
    private static final int SLIDE_END_FRAME_ROWS = 1;
    //Deadening
    private static final int DEAD_FRAME_COLS = 3;
    private static final int DEAD_FRAME_ROWS = 1;

    private Animation<TextureRegion> runAnimation;
    private TextureRegion[] fallingFrames;
    private Animation<TextureRegion> jumpAnimation;
    private Animation<TextureRegion> fallAnimation;
    private Animation<TextureRegion> slideAnimation;
    private TextureRegion[] deadFrames;
    TextureRegion[] slideEndFrame;
    private Animation<TextureRegion> deadAnimation;

    private float stateTimer = 0;
    private float holdTimer = 0;

    private PlayerState currentState;
    private PlayerState previousState;

    private boolean isDead;

    private WorldMap map;

    private Sound jumpSound;
    private Sound slideSound;
    private Sound dyingSound;

    /**
     * Creates the player a primary game object of the game
     * @param startX the starting position of the players x coordinate
     * @param startY the starting position of the players y coordinate
     * @param map the map the player will be drawn upon to check collisions with
     * Post: A player will be created with an object movement behaviour and collision object
     */
    public Player(int startX, int startY, WorldMap map) {
        super(startX, startY);
        this.map = map;
        definePlayerTextures();
        loadPlayerSounds();
        isDead = false;

        //Set the behavioiurs
        setMovementBehaviour(new ObjectMovementBehaviour<Player>(this, 0, 0));
        getMovementBehaviour().setMaxForwardVelocity(450);
        setCollisionObject(new CollisionObject(this, new Rectangle(startX, startY, 50, 100)));
    }

    /**
     * Sets up all the associated sounds with the player
     */
    private void loadPlayerSounds() {
        //Credit Freesound.org
        jumpSound = Gdx.audio.newSound(Gdx.files.internal("sounds/jump.wav"));
        slideSound = Gdx.audio.newSound(Gdx.files.internal("sounds/slide.wav"));
        dyingSound = Gdx.audio.newSound(Gdx.files.internal("sounds/dying.wav"));
    }

    /**
     * Sets up all the animations and textures
     */
    private void definePlayerTextures() {

        //Prepare our character textures for running, jumping, sliding and deadening
        Texture runSheet = new Texture(Gdx.files.internal("running.png"));
        TextureRegion[] runFrames = transform2DTextureRegionTo1D(runSheet, RUN_FRAME_COLS, RUN_FRAME_ROWS);
        runAnimation = new Animation<TextureRegion>(.2f, runFrames);


        //Jumping
        Texture jumpStartSheet = new Texture(Gdx.files.internal("jumpStartSheet.png"));
        Texture jumpFallingSheet = new Texture(Gdx.files.internal("jumpEndSheet.png"));

        TextureRegion[] jumpFrames = transform2DTextureRegionTo1D(jumpStartSheet, JUMP_START_FRAME_COLS, JUMP_START_FRAME_ROWS);
        fallingFrames = transform2DTextureRegionTo1D(jumpFallingSheet, JUMP_END_FRAME_COLS, JUMP_END_FRAME_ROWS);
        jumpAnimation = new Animation<TextureRegion>(0.1f, jumpFrames);
        fallAnimation = new Animation<TextureRegion>(.1f, fallingFrames);

        //Sliding
        Texture slideStartSheet = new Texture(Gdx.files.internal("slideStartSheet.png"));
        Texture slideEndSheet = new Texture(Gdx.files.internal("slideEndSheet.png"));
        slideEndFrame = transform2DTextureRegionTo1D(slideEndSheet, SLIDE_END_FRAME_COLS, SLIDE_END_FRAME_ROWS);
        TextureRegion[] slideFrames = transform2DTextureRegionTo1D(slideStartSheet, SLIDE_START_FRAME_COLS, SLIDE_START_FRAME_ROWS);
        slideAnimation = new Animation<TextureRegion>(0.3f, slideFrames);

        //Dying
        Texture deadSheet = new Texture(Gdx.files.internal("deadSheet.png"));
        deadFrames = transform2DTextureRegionTo1D(deadSheet, DEAD_FRAME_COLS, DEAD_FRAME_ROWS);
        deadAnimation = new Animation<TextureRegion>(0.2f, deadFrames);

        //setTexture(runSheet);
        currentState = PlayerState.RUNNING;
        setCurrentFrame(getFrame(0));
        setWidth(getCurrentFrame().getRegionWidth());
        setHeight(getCurrentFrame().getRegionHeight());

    }


    @Override
    public void act(float delta) {

        //Cap the delta so it doesn't get out of control during debugging
        if (delta == 0 || delta > 2) {
            return;
        }
        currentState = getState();
        setCurrentFrame(getFrame(delta));
        updateGameObject(delta, 0, 20);
        resizeBounds();
    }

    /**
     * Retrieves the current player state
     * @return the players current state depending on what they are doing
     */
    private PlayerState getState() {

        if (currentState!= PlayerState.DYING && movementBehaviour.getNetVerticalVelocity() > 0) {
            return PlayerState.JUMPING;
        }

        int centerOfCollisionBox = (int) getCollisionObject().getPosition().x + (int) getCollisionObject().getShapeWidth() / 2;
       // int heightOfTileUnderneath = map.getGroundHeight(centerOfCollisionBox);

        if (map.isGrounded(this, centerOfCollisionBox)) {

            movementBehaviour.setYVelocity(GameStage.GRAVITY);
            movementBehaviour.setLinearVelocity(GameStage.GRAVITY);

        } else {
             movementBehaviour.accelerateDown();
        }

        if (currentState == PlayerState.DYING) {
            return PlayerState.DYING;
        }


        if (movementBehaviour.getNetVerticalVelocity() < 0) {
           // setOrigin(getX(), heightOfTileUnderneath);

            return PlayerState.FALLING;
        }

        if (currentState == PlayerState.SLIDING) {
            getMovementBehaviour().deaccelerateX();
            return PlayerState.SLIDING;
        }

       // movementBehaviour.setLinearVelocity(GameStage.GRAVITY);
        if( currentState == PlayerState.FINISHED){
            return currentState;
        }

        getMovementBehaviour().accelerateForward();
        return PlayerState.RUNNING;
    }


    /**
     * Sets and returns the current animation frame
     * @param deltaTime change
     * @return the current animation frame
     */
    public TextureRegion getFrame(float deltaTime) {

        if (isDead) {
            return getCurrentFrame();
        }

        //Its finished running reset
        if (holdTimer < 0) {

            holdTimer = 0;

            if(currentState != PlayerState.DYING){
                currentState = PlayerState.RUNNING;
            }
        }

        //Its holding a frame, keep the current frame until complete
        if (holdTimer > 0) {
            holdTimer -= deltaTime;
            if(holdTimer < 0.3f){
                setCurrentFrame(slideEndFrame[0]);
            }
            return getCurrentFrame();
        }

        stateTimer = currentState == previousState ? stateTimer + deltaTime : 0;

        switch (currentState) {

            case DYING:

                if (deadAnimation.isAnimationFinished(stateTimer)) {
                    System.out.println("PLAYER OFFICALLY DEAD");
                    isDead = true;
                    return getCurrentFrame();
                } else {
                    setCurrentFrame(deadAnimation.getKeyFrame(stateTimer, false));
                    movementBehaviour.setXVelocity(0);
                }
                break;


            case JUMPING:
                setCurrentFrame(jumpAnimation.getKeyFrame(stateTimer, false));
                break;

            case FALLING:


                stateTimer = getY() > getOriginY() ? 0 : stateTimer;

                setCurrentFrame(fallAnimation.getKeyFrame(stateTimer, false));
                if (fallAnimation.isAnimationFinished(stateTimer)) {
                    movementBehaviour.setLinearVelocity(GameStage.GRAVITY);
                    movementBehaviour.setYVelocity(0);
                    currentState = getState();
                }

                break;


            case SLIDING:

                setCurrentFrame(slideAnimation.getKeyFrame(stateTimer, false));

               if (slideAnimation.isAnimationFinished(stateTimer) && holdTimer <= 0) {
                    holdTimer = 0.5f;
               }
                break;



            default:
                setCurrentFrame(runAnimation.getKeyFrame(stateTimer, true));
        }

        previousState = currentState;

        return getCurrentFrame();
    }


    /**
     * Begins the players death sequence
     *
     */
    public void dead() {
        dyingSound.play();
        currentState = PlayerState.DYING;
    }

    /**
     * Launches the player into the air
     */
    public void jump() {
        jumpSound.play(1.0f);
        movementBehaviour.setYVelocity(20);
    }

    /**
     * Makes the player slide
     */
    public void slide() {
        slideSound.play(1.0f);
        currentState = PlayerState.SLIDING;
    }


    /**
     * Checks to see if the player has collided with the given shape
     * @param shape to check
     * @return true if they have, false otherwise
     */
    public boolean checkCollision(Shape2D shape) {
        return getCollisionObject().checkCollision(shape);
    }

    /**
     * Checks the players current state
     * @return the players current state
     */
    public PlayerState getCurrentState() {
        return currentState;
    }

    /**
     * Modifies the bounding box so that it adjusts to the player sliding
     */
    private void resizeBounds() {
        //Modify bounding box size based on state

        if (currentState != PlayerState.SLIDING) {
            getCollisionObject().resizeBounds(50, 100);
        } else {
            getCollisionObject().resizeBounds(100, 50);
        }
    }

    /**
     * Use this to deaccelerate the player and set their current state to finished
     */
    public void atFinish() {
        getMovementBehaviour().deaccelerateX();
        currentState = PlayerState.FINISHED;

    }

    @Override
    public void dispose() {
        jumpSound.dispose();
        slideSound.dispose();
        dyingSound.dispose();

    }

    public enum PlayerState {RUNNING, JUMPING, FALLING, SLIDING, DYING, FINISHED}

    /**
     * Checks to see if the player is stil falling
     * @return true if the player is falling
     */
    public boolean inAirFalling(){
        return  currentState == PlayerState.FALLING;
    }
}
