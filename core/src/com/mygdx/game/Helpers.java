package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Helpers {

    /**
     * Transforms a 2D texture Region into a 1D array
     *
     * @param sheet   the texture sheet
     * @param columns the number of columns
     * @param rows    the number of rows
     * @return a 1 dimensional array of the sheet frames
     */
    public static TextureRegion[] transform2DTextureRegionTo1D(Texture sheet, int columns, int rows) {

        //We will use this to hold the 2d array before we convert to 1d
        TextureRegion[][] temp = TextureRegion.split(sheet, sheet.getWidth() / columns, sheet.getHeight() / rows);
        TextureRegion[] sheetFrames = new TextureRegion[columns * rows];

        int index = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                sheetFrames[index++] = temp[i][j];
            }
        }
        return sheetFrames;
    }

    public static TextureRegion[] combineBeginningAndEndRegions(TextureRegion[] start, TextureRegion[] end) {

        TextureRegion[] temp = new TextureRegion[start.length + end.length];
        System.arraycopy(start, 0, temp, 0, start.length);
        System.arraycopy(end, 0, temp, start.length, end.length);
        return temp;
    }

    /**
     * Generates the animation from a given file
     */
    public static Animation<TextureRegion> getAnimationFromTexture(String filePath, int columns, int rows, float duration) {

        Texture moveSheet = new Texture(Gdx.files.internal(filePath));
        TextureRegion[] moveFrames = transform2DTextureRegionTo1D(moveSheet, columns, rows);
        return new Animation<TextureRegion>(duration, moveFrames);
    }


}
