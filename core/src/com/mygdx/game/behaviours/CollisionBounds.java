package com.mygdx.game.behaviours;

import com.badlogic.gdx.math.Shape2D;

public interface CollisionBounds<T extends Shape2D> {

    /**
     * Centers the collision object on the object
     *
     * @param xOffset offset off the boxes x coordinates
     * @param yOffset offset of the boxes Y coordinates
     */
    void centerBounds(float xOffset, float yOffset);

    /**
     * Checks whether the object collides with the passed in shape
     *
     * @param shape a Rectangle or a Circle
     * @return true if collision occured, false otherwise
     */
    boolean checkCollision(T shape);

    /**
     * Allows a shape to be resized dynamically for activities such as crouching etc
     *
     * @param width  of the box
     * @param height of the box
     */
    void resizeBounds(float width, float height);
}
