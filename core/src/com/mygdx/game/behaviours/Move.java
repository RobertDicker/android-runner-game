package com.mygdx.game.behaviours;

public interface Move {

    /**
     * Moves the object a multipler of statetime in the x axis
     * @param deltaTime
     */
     void moveX(float deltaTime);

    /**
     * Moves the object a multiplier of statetime in the Y axis
     * @param deltaTime
     */
     void moveY(float deltaTime);




}
