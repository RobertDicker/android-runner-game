package com.mygdx.game.behaviours;

import com.mygdx.game.stages.GameStage;

public abstract class MovementBehaviour implements Move{


    //Multiplier for forward velocity
    private float XVelocity = 0;
    private float maxForwardVelocity = 0;

    //Multiplier for upward velocity
    private float yVelocity;
    private final float gravity;
    private float terminalVelocity = 30;
    private float linearVelocity;

    MovementBehaviour(){
        this.gravity = GameStage.GRAVITY;
        linearVelocity = gravity;
        this.yVelocity = 0;
    }

    /**
     * Gets the current velocity multiplier of the object
     * @return the current X velocity of the player
     */
    public float getXVelocity() {
        return XVelocity;
    }

    /**
     * Sets the current velocity multipler of the object
     * @param XVelocity the multiplier that acts upon a players forward momentum
     */
    public void setXVelocity(float XVelocity) {

        this.XVelocity = XVelocity;
        maxForwardVelocity = XVelocity;
    }

    public float getYVelocity() {
        return yVelocity;
    }

    public void setYVelocity(float yVelocity) {
        this.yVelocity = yVelocity;
    }

    /**
     * Sets the linear velocity that will act upon the user
     * @param linearVelocity is the velocity to be set
     */
    public void setLinearVelocity(float linearVelocity) {
        this.linearVelocity = linearVelocity;
    }

    /**
     * Increases the downward acceleration equal to gravity
     */
    public void accelerateDown(){
        linearVelocity += gravity;
    }

    /**
     * This is the active velocity on the player,
     * @return The velocity will be positive if the player is overcoming gravity, 0 if equal, negative otherwise.
     */
    public float getNetVerticalVelocity(){
        return this.getYVelocity() - Math.min(this.linearVelocity, terminalVelocity);
    }

    /**
     * Accelerates the player slowly forward until they meet maximum forward velocity
     */
    public void accelerateForward(){
        XVelocity = Math.min(XVelocity + 3, maxForwardVelocity);
    }

    /**
     * Deaccelerates the players X velocity to a stand still
     */
    public void deaccelerateX(){
        XVelocity = Math.max(0, XVelocity - 1);
    }

    /**
     * This sets the maximum velocity for the player altering how fast they move
     * @param maxForwardVelocity as a multiplier.
     */
    public void setMaxForwardVelocity(float maxForwardVelocity){
        this.maxForwardVelocity = maxForwardVelocity;
    }
}
