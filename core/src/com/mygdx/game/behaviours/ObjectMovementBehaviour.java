package com.mygdx.game.behaviours;

import com.badlogic.gdx.scenes.scene2d.Actor;

public class ObjectMovementBehaviour<T extends Actor> extends MovementBehaviour {

    private T gameSprite;

    /**
     * Creates an object movement behaviour that moves on the x and y access a multipler of delta
     * @param gameSprite that moves
     * @param xVelocity the multiplier for the x axis
     * @param yVelocity the initial yVelocity for the y axis
     */
    public ObjectMovementBehaviour(T gameSprite, float xVelocity, float yVelocity){
        super();
        this.gameSprite = gameSprite;
        setXVelocity(xVelocity);
        setYVelocity(yVelocity);
    }


    @Override
    public void moveX(float dT){

        gameSprite.setX(gameSprite.getX() + (getXVelocity() * dT));
    }

    @Override
    public void moveY(float dT) {

        if (getNetVerticalVelocity() != 0) {
            gameSprite.setY(Math.max(gameSprite.getY() + getNetVerticalVelocity(), 0));
            accelerateDown();
        }
    }
}
