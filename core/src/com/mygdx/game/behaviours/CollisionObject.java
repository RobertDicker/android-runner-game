package com.mygdx.game.behaviours;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Shape2D;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;


public class CollisionObject implements CollisionBounds {


    Shape2D shape;
    Actor actor;

    /**
     * This class acts as a bounding shape on the object
     * It handles the collision checking of the object, and updating of its position
     *
     * @param actor         the object this belongs too
     * @param boundingShape the shape Rectangle or Circle used for a collision object
     */
    public CollisionObject(Actor actor, Shape2D boundingShape) {
        this.actor = actor;
        this.shape = boundingShape;
    }

    public void centerBounds() {
        centerBounds(0, 0);
    }

    @Override
    public void centerBounds(float xOffset, float yOffset) {

        //Centers on the image but leaves it based on the ground
        if (shape instanceof Rectangle) {
            ((Rectangle) shape).setPosition(actor.getX() + (actor.getWidth()) / 2f - (((Rectangle) shape).getWidth() / 2) + xOffset, actor.getY() + yOffset);

        }

        //     Places circle starting from the center of the sprite
        if (shape instanceof Circle) {
            ((Circle) shape).setPosition(actor.getX() + actor.getWidth() / 2f - ((Circle) shape).radius + xOffset, actor.getY() + (actor.getWidth() / 2f) - ((Circle) shape).radius + yOffset);

        }
    }


    @Override
    public boolean checkCollision(Shape2D otherShape) {

        if (shape instanceof Rectangle && otherShape instanceof Rectangle) {
            return Intersector.overlaps((Rectangle) shape, (Rectangle) otherShape);
        }

        if (shape instanceof Rectangle && otherShape instanceof Circle) {
            return Intersector.overlaps((Circle) otherShape, (Rectangle) shape);
        }

        if (shape instanceof Circle && otherShape instanceof Circle) {
            return Intersector.overlaps((Circle) shape, (Circle) otherShape);
        }

        return Intersector.overlaps((Circle) shape, (Rectangle) otherShape);
    }


    @Override
    public void resizeBounds(float width, float height) {

        if (shape instanceof Rectangle) {
            ((Rectangle) shape).setSize(width, height);
        }
        if (shape instanceof Circle) {
            ((Circle) shape).setRadius(width * 2);
        }
    }


    public Shape2D getBoundingShape() {
        return shape;
    }

    public Vector2 getPosition() {

        if (shape instanceof Rectangle) {
            return new Vector2(((Rectangle) shape).x, ((Rectangle) shape).y);
        }

        if (shape instanceof Circle) {
            return new Vector2(((Circle) shape).x, ((Circle) shape).y);
        }
        return new Vector2();
    }


//    public float getShapeX(){
//
//        if(shape instanceof Rectangle){
//            return ((Rectangle) shape).x;
//        }
//
//        if(shape instanceof Circle){
//            return ((Circle) shape).x;
//        }
//        return 0;
//    }
//
//    public float getShapeY(){
//
//        if(shape instanceof Rectangle){
//            return ((Rectangle) shape).y;
//        }
//
//        if(shape instanceof Circle){
//            return ((Circle) shape).y;
//        }
//        return 0;
//    }

    public float getShapeWidth() {

        if (shape instanceof Rectangle) {
            return ((Rectangle) shape).width;
        }

        if (shape instanceof Circle) {
            return ((Circle) shape).radius * 2;
        }
        return 0;
    }

    public float getShapeHeight() {

        if (shape instanceof Rectangle) {
            return ((Rectangle) shape).getHeight();
        }

        if (shape instanceof Circle) {
            return ((Circle) shape).radius * 2;
        }
        return 0;
    }


}
