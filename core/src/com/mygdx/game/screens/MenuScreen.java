package com.mygdx.game.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.PlatformerGame;

public class MenuScreen implements Screen {

    BitmapFont font;
    Label.LabelStyle labelStyle;
    PlatformerGame game;
    Stage stage;
    private Skin skin;

    /**
     * The main constructor for the main menu
     * @param game a reference to the parent game class
     * @param viewport the viewport this will run off
     */
    public MenuScreen(PlatformerGame game, Viewport viewport){
        this.game = game;
        this.stage = new Stage(viewport);
    }


    @Override
    public void show() {
        createMenu();
        Gdx.input.setInputProcessor(stage);
        render(0);
    }

    /**
     * Creates the Main menu
     */
    private void createMenu() {

        BitmapFont font;
        font  = new BitmapFont(Gdx.files.internal("skin/font-export.fnt"));

        labelStyle = new Label.LabelStyle(font, Color.WHITE);
        skin = new Skin(Gdx.files.internal("skin/star-soldier-ui.json"));

        //Changes the screen to the game screen
        TextButton play = createButton("Play") ;
        play.addListener(new ClickListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {

               game.setGameScreen();
               return true;
            }
        });

        //Exits the game
        TextButton exit = createButton("Exit");
        exit.addListener(new ClickListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                game.dispose();
                return true;
            }
        });


        float w = stage.getViewport().getWorldWidth();
        float h = stage.getViewport().getWorldHeight();

        Image image = new Image(new Texture(Gdx.files.internal("slideEndSheet.png")));
        image.scaleBy(1f);
        Label label  = new Label("Crash Dummy Runner", labelStyle);
        label.setFontScale(2f);


        Table table = new Table();
        table.setFillParent(true);

        table.add(image);
        table.row();
        table.add(label).expandY().center().colspan(2);
        table.row();
        table.add(play).width(w/2).height(h/4);
        table.bottom();
        table.add(exit).width(w/2).height(h/4);

        stage.addActor(table);


    }

    /**
     * A helper method to create uniform buttons on the main menu
     * @param label
     * @return a text button scale 4 using the provided skin
     */
    private TextButton createButton( String label){
        TextButton tb =  new TextButton(label, skin);
        tb.getLabel().setFontScale(4);
        return tb;
    }

    @Override
    public void render(float delta) {

        Gdx.gl20.glClearColor(0, 0, 0, 1);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void dispose() {

        stage.dispose();
    }


}
