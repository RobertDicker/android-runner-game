package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.PlatformerGame;
import com.mygdx.game.scenes.Hud;
import com.mygdx.game.stages.GameStage;

import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;

public class GameScreen implements Screen {

    private boolean loaded;
    private float delay = 1.5f;
    private final GameStage gameStage;
    private final PlatformerGame game;

    public GameScreen(PlatformerGame game) {
       this.game = game;
       gameStage = new GameStage(this);
       loaded = true;
    }

    public void render(float deltaTime) {
        if(!loaded){return;}
        if(delay > 0){
          delay -= Math.min(deltaTime, 1);
          deltaTime = 0;
        }

        deltaTime = gameStage.isPaused() ? 0 : deltaTime;

        Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        gameStage.drawMap();
        gameStage.update(deltaTime);

        gameStage.act(deltaTime);
        gameStage.draw();
    }


    @Override
    public void dispose() {
        gameStage.dispose();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {
    }

    @Override
    public void show() {
        Gdx.app.log("GameScreen: ", "gameScreen show called");
        gameStage.restartLevel();
    }

    @Override
    public void hide() {
        Gdx.app.log("GameScreen: ", "gameScreen hide called");

    }

    public void exitToMenu(){
        game.setMainMenuScreen();
    }

    public Viewport getGameScreenViewport(){
        return game.getGameViewPort();
    }




}
