package com.mygdx.game.stages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.mygdx.game.actors.Monster;
import com.mygdx.game.MonsterBuilder;
import com.mygdx.game.Player;
import com.mygdx.game.actors.WorldMap;
import com.mygdx.game.effects.ParticleEffectActor;
import com.mygdx.game.scenes.Hud;
import com.mygdx.game.screens.GameScreen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;

public class GameStage extends Stage {

    private GameScreen screen;
    public static final float GRAVITY = .98f;
    private static final boolean COLLISION_DEBUG = false;
    private final ArrayList<WorldMap> levels;
    //Enemy
    private final MonsterBuilder mb;
    private final HashMap<Integer, List<Float>> enemyLocations;
    private OrthographicCamera camera;
    private ParticleEffectActor particleEffectactor;
    private ShapeRenderer shapeRenderer;
    private Hud hud;
    private int LEVEL_FINISH_X;
    private int currentScore = 0;
    private int currentHighScore = 0;
    private int level = 0;
    private GameState currentGameState;
    //World
    private WorldMap map;
    //Player
    private Player player;
    private PriorityQueue<Monster> activeMonster;

    private Music gameMusic;



    /**
     * Creates an instance of game stage, the primary stage for playing the game
     *
     * @param screen is the screen the gamestage is loaded in
     */
    public GameStage(GameScreen screen) {
        super(screen.getGameScreenViewport());
        this.screen = screen;
        levels = new ArrayList<WorldMap>();

        //The prototype builder for creating monsters to use
        mb = new MonsterBuilder();

        // Enemies are setup by their location, and the last digit determines their type, see monster builder
        enemyLocations = new HashMap<Integer, List<Float>>();
        enemyLocations.put(enemyLocations.size(), Arrays.asList(2000f, 4001f, 5200f, 5700f, 7000f, 8001f, 9000f, 10000f, 12001f, 13000f, 13500f, 14000f, 15500f, 16300f));
        enemyLocations.put(enemyLocations.size(), Arrays.asList(2501f, 8000f, 9800f, 7002f, 6003f,  10502f, 11001f, 11503f, 12000f, 12501f));

    }

    /**
     * Loads or reloads the stage, setting up the current actors
     */
    private void loadNewStage() {
        setupCamera();

        setupMap();

        setupPlayer();

        setupMonsters();

        hud = new Hud(this, currentScore, currentHighScore);

        setupControls();
        currentGameState = GameState.MENU;
        shapeRenderer = new ShapeRenderer();
        particleEffectactor = new ParticleEffectActor(5);
        addActor(particleEffectactor);
        loadGameMusic();

    }

    /**
     * Plays the games sound track
     */
    private void loadGameMusic() {

        if(gameMusic == null){
            gameMusic = Gdx.audio.newMusic(Gdx.files.internal("sounds/gamemusic.wav"));
        }

        if(!gameMusic.isPlaying()){
            gameMusic.play();
        }

        gameMusic.setLooping(true);

    }

    /**
     * This creates an orhographic camera and aligns the camera
     */
    private void setupCamera() {

        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();


        camera = (OrthographicCamera) this.getViewport().getCamera();
        camera.setToOrtho(false, w / 2, h / 2);
        camera.position.y = getViewport().getWorldHeight() / 2 + 380;
        camera.update();
    }

    /**
     * Sets up the maps as actors in the stage
     */
    private void setupMap() {

        //Create the levels
        if (levels.isEmpty()) {
            levels.add(new WorldMap("level1.tmx"));
            levels.add(new WorldMap("level2.tmx"));
        }

        map = levels.get(level);
        addActor(map);

        LEVEL_FINISH_X = map.getExitPoint();
    }

    /**
     * This creates the monsters
     * The builder uses a modulous to determine monster type so monsters are placed based on location
     */
    private void setupMonsters() {

        activeMonster = new PriorityQueue<Monster>();

        //Create the enemies
        for (float position : enemyLocations.get(level)) {

            Monster monster = mb.getMonster((int) position % 10);
            monster.setStartingXPosition(position);
            addActor(monster);
            activeMonster.add(monster);
        }
    }

    /**
     * This handles player creation and recreation, if the player doesnt exist it creates one
     */
    private void setupPlayer() {
        //Purge previous player
        if (player != null) {
            player.getCurrentFrame().getTexture().dispose();
            player = null;
        }
        player = new Player(150, 407, map);
        addActor(player);
    }

    /**
     * This sets up the input controls for the player, the top half of the screen for jump, the bottom for slide
     */
    private void setupControls() {

        //I have used a multiplexer here to separate the controls of the gamescreen from the hud
        InputMultiplexer multiplexer = new InputMultiplexer();

        multiplexer.addProcessor(hud.getStage());
        multiplexer.addProcessor(new InputAdapter() {

            @Override
            public boolean touchDown(int screenX, int screenY, int pointer, int button) {

                //Do nothing when paused
                if (hud.pauseButtonIsPressed()) {

                    return false;
                }

                Vector3 touchPosition = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);

                //If the player is in already in an active state ignore
                if (player.getCurrentState() != Player.PlayerState.RUNNING) {
                    return false;
                }

                if (touchPosition.y < Gdx.graphics.getHeight() / 2f) {
                    player.jump();
                } else {
                    player.slide();
                }
                return true;
            }
        });

        Gdx.input.setInputProcessor(multiplexer);
    }

    public void update(float deltaTime) {

        currentGameState = getCurrentGameState();



        switch (currentGameState) {

            // Player has failed, reset the scores, kill the player and display the options menu
            case FAILED: {
                currentHighScore = hud.getHighscore();
                currentScore = 0;
                player.dead();
                hud.showOptionsMenu("Game Over");
                break;
            }

            //Player wins scenario, deaccelerate player, and start fireworks
            case COMPLETE: {
                player.atFinish();
                currentGameState = GameState.ENDSEQUENCE;
                particleEffectactor.prepareFireworks();
                break;
            }

            // Fireworks have been completed, proceed to the next level if there is one
            case ENDSEQUENCECOMPLETED: {

                level++;
                if (level < levels.size()) {
                    currentHighScore = hud.getHighscore();
                    currentScore = hud.getScore();
                    this.clear();
                    loadNewStage();

                } else {


                        hud.showOptionsMenu("You Win!");
                        level = 0;
                        currentHighScore = hud.getHighscore();
                        currentScore = hud.getScore();

                }
                break;
            }

            //A catchall to prevent the camera moving when the options menu is up
            case OPTIONS: {
                break;
            }

            default:
                if(deltaTime != 0){
                    moveCamera();
                }

        }

        getBatch().setProjectionMatrix(camera.combined);
        shapeRenderer.setProjectionMatrix(camera.combined);
        updateMonsterPositions(deltaTime);
        hud.update(player.getX());
        displayCollisionBoxes();

    }

    /**
     * This is for debugging, to view the collision boxes of the monster and player
     * To set change the constant COLLISION_DEBUG to true
     */
    private void displayCollisionBoxes() {

        if (!COLLISION_DEBUG) {
            return;
        }

        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);

        shapeRenderer.rect(player.getCollisionObject().getPosition().x, player.getCollisionObject().getPosition().y, player.getCollisionObject().getShapeWidth(), player.getCollisionObject().getShapeHeight());

        for (Monster monster : activeMonster) {
            shapeRenderer.circle(monster.getCollisionObject().getPosition().x, monster.getCollisionObject().getPosition().y, monster.getCollisionObject().getShapeWidth(), (int) monster.getCollisionObject().getShapeHeight());
        }

        shapeRenderer.rect(map.getRectangle().x, map.getRectangle().y, map.getRectangle().getWidth(), map.getRectangle().getHeight());

        shapeRenderer.end();

    }

    private void updateMonsterPositions(float deltaTime) {

        //Updates their position
        for (Monster monster : activeMonster) {
            if(monster.getX() - player.getX() < 1400){
                monster.update(deltaTime);
            }
        }

        //Remove the monster from active play if its offscreen
        if (activeMonster.size() > 0) {
            if (activeMonster.peek().getX() <= player.getX() - getViewport().getWorldWidth()) {
                activeMonster.poll();
            }
        }
    }

    /**
     * Checks the player against the collision box of monsters
     *
     * @return true if there was a collision false otherwise
     */
    private boolean checkCollisions() {
        for (Monster monster : activeMonster) {

            //Skip the monster if its behind the player, no need to check
            //Note: The game will pop the monster off the priority queue as they leave the screen
            //However this condition ensures that if there are more than one on screen, it will check those infront
            if (monster.getY() + player.getWidth() < player.getY()) {
                continue;
            }

            if (player.checkCollision(monster.getCollisionBox())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks to see if the game is paused
     *
     * @return true if the game is paused, false otherwise
     */
    public boolean isPaused() {
        return currentGameState == GameState.PAUSED;
    }

    /**
     * Clears the stage and restarts the level loading new stage
     */
    public void restartLevel() {

        this.clear();
        loadNewStage();
        currentGameState = GameState.PLAYING;
    }

    public void loadMainMenu(){
        gameMusic.stop();
      //  gameMusic.dispose();
        this.clear();
        this.screen.exitToMenu();
    }

    /**
     * Moves the camera in line with the player
     * This is a self centering camera, so that when the player jumps up it moves up to align with them and down to align with them
     * This is done because as a player falls the ground goes offscreen, so this adds a bit of fall to the player
     */
    private void moveCamera() {

        camera.position.x = getViewport().getWorldWidth() / 2 + player.getX();
        if (player.inAirFalling()) {
            camera.position.y = Math.min(camera.position.y - GameStage.GRAVITY * 3, player.getY() + getViewport().getWorldHeight() / 2);
        } else {
            camera.position.y = Math.min(camera.position.y + GameStage.GRAVITY * 4, player.getY() + getViewport().getWorldHeight() / 2);
        }

        camera.update();
    }

    /**
     * Draws the map
     */
    public void drawMap() {
        map.draw();
    }

    public GameState getCurrentGameState() {

        if (hud.isPaused()) {
            return GameState.PAUSED;
        }

        if (hud.isOptionsMenuShown()) {
            return GameState.OPTIONS;
        }
        if (particleEffectactor.isCompleted()) {
            return GameState.ENDSEQUENCECOMPLETED;
        }



        if (checkCollisions() && currentGameState == GameState.PLAYING) {
             return GameState.FAILED;
        }

        if (player.getY() < 200) {
            return GameState.FAILED;
        }

        if (getCamera().position.x > LEVEL_FINISH_X) {
            return GameState.COMPLETE;
        }

        return GameState.PLAYING;
    }

    public enum GameState {PLAYING, COMPLETE, PAUSED, MENU, FAILED, OPTIONS, ENDSEQUENCE, ENDSEQUENCECOMPLETED}
}
