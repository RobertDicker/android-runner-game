package com.mygdx.game.effects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.math.RandomXS128;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class ParticleEffectActor extends Actor {


   private ParticleEffect particleEffect;
//    private int particleCounter;
    private boolean active;
    private boolean completed;
    private int particalCounter;
    private Sound particalSound;

    float w;
    float h;

    /**
     * Creates a partical effect of a firework that goes off the specified number of times
     * @param numberOfTimes that you wish the effect to last, to start call prepareFireworks
     */
    public ParticleEffectActor(int numberOfTimes){

        active =false;
        completed = false;

        particalCounter = numberOfTimes;
        particleEffect = new ParticleEffect();
        particleEffect.load(Gdx.files.internal("particles/star.p"),Gdx.files.internal("particles/"));
        particleEffect.start();
        particalSound = Gdx.audio.newSound(Gdx.files.internal("sounds/fireworks.wav"));
        particalSound.play(1.0f);
    }

    /**
     * Prepares the fireworks, and sets them active to be drawn
     * once this is called the fireworks will be drawn until particalCounter hits 0
     */
    public void prepareFireworks() {

        w = getStage().getViewport().getWorldWidth();
        h = getStage().getViewport().getWorldHeight();
        active = true;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        if(!active){return;}

        if(particalCounter < 0) {
            completed = true;
            return;}


            if(particleEffect.isComplete()){
                particleEffect.start();
                particalSound.play(1.0f);
                particalCounter--;
            }

        particleEffect.draw(batch);

    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if(!active){return;}

        float xMin = getStage().getCamera().position.x - w/2f;
        float yMin = getStage().getCamera().position.y + h/2f;

        RandomXS128 randomXS128 = new RandomXS128();
        randomXS128.setSeed(randomXS128.nextInt());
        particleEffect.setPosition(xMin+50 + (randomXS128.nextFloat() * w) ,  yMin  );
        particleEffect.update(delta);

    }

    /**
     * Checks to see if the fireworks have completed
     * @return true if they have, false otherwise
     */
    public boolean isCompleted() {
        return completed;
    }


}
