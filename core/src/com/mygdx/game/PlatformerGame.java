package com.mygdx.game;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.screens.GameScreen;
import com.mygdx.game.screens.MenuScreen;

public class PlatformerGame extends Game implements ApplicationListener {

    public static final int GAME_WORLD_WIDTH = 800;
    public static final int GAME_WORLD_HEIGHT = 480;
    public static Viewport ui;
    private static MenuScreen mainMenu;
    private final Viewport gameViewPort = new StretchViewport(GAME_WORLD_WIDTH, GAME_WORLD_HEIGHT);
    private GameScreen gameScreen;

    @Override
    public void create() {
        Gdx.app.log("MyGdxGame: ", "Starting Application");

        ui = new StretchViewport(GAME_WORLD_WIDTH, GAME_WORLD_HEIGHT);
        gameScreen = new GameScreen(this);
        mainMenu = new MenuScreen(this, ui);
        setScreen(mainMenu);
    }

    @Override
    public void dispose() {
        super.dispose();
        gameScreen.dispose();
        mainMenu.dispose();
        Gdx.app.exit();
        System.exit(1);
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }

    public void setGameScreen() {
        setScreen(gameScreen);
    }

    public void setMainMenuScreen() {
        setScreen(mainMenu);
    }


    public Viewport getGameViewPort() {
        return gameViewPort;
    }
}
