package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.RandomXS128;
import com.mygdx.game.actors.Bat;
import com.mygdx.game.actors.Monster;
import com.mygdx.game.actors.Slime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import static com.mygdx.game.Helpers.getAnimationFromTexture;

public class MonsterBuilder {

    List<Monster> monsterPrototypes;
    HashMap<String, Animation<TextureRegion>> monsterTexturesCache;

    /**
     * A prototype class, creates monsters to be cloned
     */
    public MonsterBuilder() {

        loadTextures();
        monsterPrototypes = new ArrayList<Monster>();
        monsterPrototypes.add(new Slime(monsterTexturesCache.get("Slime"), Gdx.graphics.getWidth() * -1, 420));
        monsterPrototypes.add(new Bat(monsterTexturesCache.get("Bat"), Gdx.graphics.getWidth() * -1, 460));
    }

    /**
     * Storing the textures in a hashmap as a cache of animations so that we are only loading once.
     */
    private void loadTextures() {

        monsterTexturesCache = new HashMap<String, Animation<TextureRegion>>();
        monsterTexturesCache.put("Bat",  Helpers.getAnimationFromTexture("bat.png",4,1, 0.2f));
        monsterTexturesCache.put("Slime", Helpers.getAnimationFromTexture("slime.png",2, 1, 0.2f));
    }

    /**
     * Gets a random monster from the monsters list     *
     * @return a random monster
     */
    public Monster getRandomMonster() {
        Random random = new RandomXS128();
        return monsterPrototypes.get(random.nextInt(monsterPrototypes.size())).deepClone();
    }


    /**
     * Gets a specific type of monster
     *
     * @param type 1: Slime, 2: Bat, 3: Higher Bat, 4: Highest Bat
     * @return a monster
     */
    public Monster getMonster(int type) {
        Monster monster;
        switch (type) {

            case 1:
                //Default height Bat
                return monsterPrototypes.get(1).deepClone();
            case 2:
                monster = monsterPrototypes.get(1).deepClone();
                monster.setStartingXPosition(560);
                return monster;
            case 3:
                monster = monsterPrototypes.get(1).deepClone();
                monster.setStartingXPosition(600);
               return monster;

            default:
                return monsterPrototypes.get(0).deepClone();
        }
    }
}
