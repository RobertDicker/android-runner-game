package com.mygdx.game.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Disposable;
import com.mygdx.game.PlatformerGame;
import com.mygdx.game.stages.GameStage;

public class Hud implements Disposable {


    private final Stage stage;
    private final Table table;
    private final Label.LabelStyle style;
    private final Label highScoreLabel;
    private final Label distanceLabel;
    private final Label fpsLabel;
    private final Label pauseLabel;
    private final TextButton pauseButton;
    private final long pauseCooldownMillis;
    private final GameStage primaryStage;
    private int score;
    private int distance;
    private Label alertLabel;
    private boolean paused;
    private final BitmapFont font;
    private long pauseTime;
    private Table optionsTable;
    private boolean optionsMenuShown;
    private int formattedDistance;

    /**
     * This is the hud for the game that displays the score and FPS
     * @param primaryStage is the game stage
     * @param currentScore is the players current score used when transitioning worlds
     * @param highscore is the players highest score
     */
    public Hud(GameStage primaryStage, int currentScore, int highscore) {

        this.primaryStage = primaryStage;
        this.distance = currentScore;
        this.score = highscore;
        paused = false;
        pauseCooldownMillis = 500;
        optionsMenuShown = false;

        stage = new Stage(PlatformerGame.ui, primaryStage.getBatch());

        //The table that will hold all of the HUD set to the top
        table = new Table();
        table.top();
        table.setFillParent(true);

        //Labels
        font = new BitmapFont();
        style = new Label.LabelStyle(font, Color.WHITE);

        highScoreLabel = new Label(score + "m", style);
        distanceLabel = new Label(distance + "m", style);
        fpsLabel = new Label(60 + "fps", style);

        pauseLabel = new Label("PAUSED", style);
        pauseLabel.setFontScale(4, 4);

        Skin skin = new Skin(Gdx.files.internal("gui/uiskin.json"));
        pauseButton = new TextButton(" || ", skin);
        pauseButton.addListener(new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {

                long time = System.currentTimeMillis();

                if (time - pauseTime < pauseCooldownMillis) {
                    return false;
                }

                paused = !paused;
                if (paused) {
                    table.row();
                    table.add(pauseLabel).colspan(3).center();
                    pauseTime = System.currentTimeMillis();
                } else {
                    table.removeActor(pauseLabel);
                }
                return true;
            }
        });


        table.add(fpsLabel).expandX().padTop(10);
        table.add(distanceLabel).expandX().padTop(10);
        table.add(highScoreLabel).expandX().padTop(10);
        table.add(pauseButton).padRight(10).padTop(10);

        stage.addActor(table);

        createOptionsTable();
    }

    public boolean isPaused() {
        return paused;
    }

    /**
     * Updates the hud, checks the frame rate, the players current distance and updates highscore if greater
     * @param playerX the players current X position
     */
    public void update(float playerX) {

        distance += playerX;
        formattedDistance = MathUtils.round(distance *.00002f);
        score = MathUtils.round(Math.max(score, formattedDistance));
        highScoreLabel.setText("Highscore: " + score + "m");
        distanceLabel.setText("Distance: " + formattedDistance + "m");
        fpsLabel.setText(Gdx.graphics.getFramesPerSecond() + "fps");
        table.setX(stage.getCamera().position.x - stage.getViewport().getWorldWidth() / 2);

        if (stage != null) {
            stage.draw();
        }
    }

    @Override
    public void dispose() {
        font.dispose();
        stage.dispose();
    }

    public boolean pauseButtonIsPressed() {
        return pauseButton.isPressed();
    }

    /**
     * Displays an option menu with the given string on the screen
     * The options are Retry, and Menu
     * @param alert is the message to display
     */
    public void showOptionsMenu(String alert) {
        table.setVisible(false);
        alertLabel.setText(alert);
        stage.addActor(optionsTable);
        optionsMenuShown = true;
    }

    /**
     * Sets up the options table and adds the retry and menu buttons to the screen
     */
    public void createOptionsTable() {

        optionsTable = new Table();
        optionsTable.center();
        optionsTable.setFillParent(true);

        Skin skin = new Skin(Gdx.files.internal("gui/uiskin.json"));
        Button retryButton = new TextButton(" Retry ", skin);
        retryButton.addListener(new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {

                primaryStage.restartLevel();

                return true;
            }
        });

        Button returnToMenuButton = new TextButton(" Menu ", skin);
        returnToMenuButton.addListener(new ClickListener() {


            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {

                        primaryStage.loadMainMenu();

                return true;
            }

        });

        alertLabel = new Label("", style);
        alertLabel.setFontScale(4, 4);
        optionsTable.add(alertLabel).colspan(2).center();
        optionsTable.row();
        optionsTable.add(retryButton).padRight(50);
        optionsTable.add(returnToMenuButton);

    }

    /**
     * Gets the overall highscore
     *
     * @return the highscore
     */
    public int getHighscore() {
        return score;
    }

    /**
     * Gets the players current score
     *
     * @return the players current score
     */
    public int getScore() {
        return distance;
    }

    /**
     * Check to see if the options menu is currently shown by the hud
     * @return true if it is false otherwise
     */
    public boolean isOptionsMenuShown() {
        return optionsMenuShown;
    }

    public Stage getStage() {
        return stage;
    }

}
