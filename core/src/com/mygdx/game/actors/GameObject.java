package com.mygdx.game.actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.mygdx.game.behaviours.CollisionObject;
import com.mygdx.game.behaviours.MovementBehaviour;

public class GameObject extends Actor {


    protected MovementBehaviour movementBehaviour;
    private TextureRegion currentFrame;
    private CollisionObject collisionObject;

    /**
     * Represents the actors of the game, the players the monsters etc
     *
     * @param startX the starting position of the object
     * @param startY the starting Y position of the object
     *               Note you should set the MovementBehaviours and the Collision objects if relevant
     */
    public GameObject(float startX, float startY) {
        this.setPosition(startX, startY);
    }

    public MovementBehaviour getMovementBehaviour() {
        return movementBehaviour;
    }

    /**
     * Sets the movement behaviour of the object
     * @param movementBehaviour to be set to the object
     */
    public void setMovementBehaviour(MovementBehaviour movementBehaviour) {
        this.movementBehaviour = movementBehaviour;
    }

    public CollisionObject getCollisionObject() {
        return collisionObject;
    }

    /**
     * Sets the collision object of the game object
     * @param collisionObject
     */
    public void setCollisionObject(CollisionObject collisionObject) {
        this.collisionObject = collisionObject;
    }

    /**
     * Basic for when no offsets are required for the collision object
     * @param deltaTime the time
     */
    public void updateGameObject(float deltaTime) {
        updateGameObject(deltaTime, 0, 0);
    }

    /**
     * Updates the games objects movement and collision behaviour/objects
     * @param deltaTime
     * @param xBoundOffset the x offset for the collision objects center
     * @param yBoundOffset the y offset for the collision objects center
     */
    public void updateGameObject(float deltaTime, float xBoundOffset, float yBoundOffset) {
        movementBehaviour.moveX(deltaTime);
        movementBehaviour.moveY(deltaTime);
        collisionObject.centerBounds(xBoundOffset, yBoundOffset);
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        if (currentFrame != null) {
            batch.draw(currentFrame, getX(), getY());
        }
    }

    /**
     * Gets the game objects current texture frame
     * @return the current image that is to be drawn
     */
    public TextureRegion getCurrentFrame() {
        return currentFrame;
    }

    public void setCurrentFrame(TextureRegion currentFrame) {
        this.currentFrame = currentFrame;
    }


}
