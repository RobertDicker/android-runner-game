package com.mygdx.game.actors;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.mygdx.game.Helpers;
import com.mygdx.game.behaviours.CollisionObject;
import com.mygdx.game.behaviours.ObjectMovementBehaviour;
import com.mygdx.game.stages.GameStage;

public class Bat extends Monster {

    private final Animation<TextureRegion> batAnimation;
    private float stateTimer;

    /**
     * Copy constructor for bat
     *
     * @param bat to clone
     */
    private Bat(Bat bat) {
        this(bat.getBatAnimation(), bat.getX(), bat.getY());
        this.stateTimer = 0;
    }


    /**
     * Creates a bat at given position
     *
     * @param startingX The starting X position
     * @param startingY The starting Y position
     */
    public Bat(Animation<TextureRegion> animation, float startingX, float startingY) {
        super(startingX, startingY);
        setMovementBehaviour(new ObjectMovementBehaviour<Monster>(this, -150, 0));
        getMovementBehaviour().setYVelocity(GameStage.GRAVITY);
        setCollisionObject(new CollisionObject(this, new Circle(startingX, startingY, 10)));

        batAnimation = animation;
        setCurrentFrame(batAnimation.getKeyFrame(0));
        setBounds(startingX, startingY, getCurrentFrame().getRegionWidth(), getCurrentFrame().getRegionHeight());
    }

    @Override
    public void update(float deltaTime) {
        stateTimer += deltaTime;
        setCurrentFrame(batAnimation.getKeyFrame(stateTimer, true));
        updateGameObject(deltaTime, 0, 15);
    }

    @Override
    public Monster deepClone() {
        return new Bat(this);
    }

    public Animation<TextureRegion> getBatAnimation() {
        return batAnimation;
    }
}
