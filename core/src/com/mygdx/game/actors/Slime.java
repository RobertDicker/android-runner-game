package com.mygdx.game.actors;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.mygdx.game.behaviours.CollisionObject;
import com.mygdx.game.behaviours.ObjectMovementBehaviour;
import com.mygdx.game.stages.GameStage;

import static com.mygdx.game.Helpers.getAnimationFromTexture;

public class Slime extends Monster implements Comparable<Monster> {

    private float stateTimer;
    private final Animation<TextureRegion> slimeAnimation;


    private Slime(Slime slime){
        this(slime.getSlimeAnimation(), slime.getX(), slime.getY());
        this.stateTimer = 0;
    }

    public Slime(Animation<TextureRegion> animation, float startingX, float startingY){
        super(startingX, startingY);
        setMovementBehaviour(new ObjectMovementBehaviour<Slime>(this, -150, 0));
       getMovementBehaviour().setYVelocity(GameStage.GRAVITY);

        setCollisionObject(new CollisionObject(this, new Circle(startingX, startingY, 20)));

        slimeAnimation = animation;
        setCurrentFrame(slimeAnimation.getKeyFrame(0));
        setBounds(startingX, startingY, getCurrentFrame().getRegionWidth(), getCurrentFrame().getRegionHeight());
    }

    @Override
    public void update(float deltaTime) {
        stateTimer+= deltaTime;
        setCurrentFrame(slimeAnimation.getKeyFrame(stateTimer, true));
        updateGameObject(deltaTime,  -20, -35);
    }

    @Override
    public Monster deepClone() {
        return new Slime(this);

    }

    public Animation<TextureRegion> getSlimeAnimation() {
        return slimeAnimation;
    }
}
