package com.mygdx.game.actors;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;

import java.util.HashMap;

public class WorldMap extends Actor {


    //Map
    private final TiledMap tiledMap;
    private final OrthogonalTiledMapRenderer tiledMapRenderer;
    private final TiledMapTileLayer collisionLayer;
    private final HashMap<Integer, Integer> ground;
    private int endTile;
    private final Rectangle groundRectangle;

    /**
     * Creates a world map actor, bsed on the given file path of the level TMX
     * @param levelTMX the path of the TMX
     * Pre: The tile map should have a layer called 'ground' and layer called 'Detail'
     */
    public WorldMap(String levelTMX) {
        tiledMap = new TmxMapLoader().load(levelTMX);
        tiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap);

        ground = new HashMap<Integer, Integer>();

        collisionLayer = (TiledMapTileLayer) tiledMap.getLayers().get("Ground");

        groundRectangle = new Rectangle(0, 420, 70, 25);
        loadImportantTileLocations();
        //This finds the end

    }

    /**
     * Finds the important tiles form the Detail layer
     * Pre: Map must have a layer named exactly 'Detail'
     * Pre: The map exit tile should have a custom property named 'exit'
     * Pre: The maps ground tiles should have a custom property named 'ground'
     * Post: The tiles will all be stored in a hash map for comparing against
     */
    private void loadImportantTileLocations(){

        TiledMapTileLayer detailLayer = (TiledMapTileLayer) tiledMap.getLayers().get("Detail");
        TiledMapTileLayer.Cell detailCell;
        TiledMapTileLayer.Cell collisionCell;

        for (int i = 0; i < collisionLayer.getWidth(); i++) {
            for (int j = 0; j < collisionLayer.getHeight(); j++) {

                detailCell = detailLayer.getCell(i, j);

                if (detailCell != null) {
                    if (detailCell.getTile().getProperties().containsKey("exit")) {
                        endTile = i;
                        System.out.println("FOUND AT " + i);
                    }
                }

                collisionCell = collisionLayer.getCell(i, j);
                if (collisionCell != null) {
                    if (collisionCell.getTile().getProperties().containsKey("ground")) {
                        ground.put(i, j);
                    }
                }
            }
        }
    }

    /**
     * Draws the given map
     */
    public void draw() {
        tiledMapRenderer.setView((OrthographicCamera) getStage().getCamera());
        tiledMapRenderer.render();
    }

    /**
     * Gets the exit point, (The point that the tile with 'exit' property is)
     * @return the exit point in current pixels given 70 tile width
     */
    public int getExitPoint() {
        return endTile * 70;
    }

    /**
     * Gets the height of the current tile
     * @param x the position of the tile
     * @return the Y value of the height in pixels
     */
    public int getGroundHeight(int x) {

        Integer height = ground.get(x / 70);
        return height == null ? 419 : ((height + 1) * 70) - 1;

    }

    /**
     * Checks to see if the object passed is touching the ground
     * @param object to check for ground collision
     * @param x the tile to check
     * @return true if touching ground, false otherwise
     */
    public boolean isGrounded(GameObject object, int x) {

       groundRectangle.setPosition(x, getGroundHeight(x)- (groundRectangle.getHeight()*0.5f));
       return groundRectangle.contains(x, object.getCollisionObject().getPosition().y);

    }

    /**
     * Gets the ground tiles collision box
     * @return the Rectangle collision box for the current forward ground rectangle
     */
    public Rectangle getRectangle() {
        return  groundRectangle;
    }
}
