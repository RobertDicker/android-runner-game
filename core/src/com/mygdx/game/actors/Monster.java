package com.mygdx.game.actors;

import com.badlogic.gdx.math.Shape2D;

public abstract class Monster extends GameObject implements Comparable<Monster>, Cloneable{

    public Monster(float startingX, float startingY){
        super(startingX, startingY);
    }

    abstract public void update(float stateTime);

    public void setStartingXPosition(float x){
        this.setX(x);
    }

    public abstract Monster deepClone();

    /**
     * Gets the bats collisionBox
     * @return a Shape2D collisionBox
     */
    public  Shape2D getCollisionBox(){
        return getCollisionObject().getBoundingShape();
    }

    /**
     * Compares the bats positions and returns its position in comparison to another monster
     * @param monster to compare X position too
     * @return  1 if  greater  0 if same -1 if less
     */
    public int compareTo(Monster monster) {
        return Float.compare(getX(), monster.getX());
    }


}
