<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.5.0" name="tiles_spritesheet" tilewidth="70" tileheight="70" spacing="2" tilecount="156" columns="12">
 <image source="tiles_spritesheet.png" width="914" height="936"/>
 <tile id="7">
  <properties>
   <property name="ground" value="true"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="2" x="1" y="0" width="70" height="19">
    <properties>
     <property name="ground" value="true"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="64">
  <properties>
   <property name="exit" value="true"/>
  </properties>
 </tile>
</tileset>
